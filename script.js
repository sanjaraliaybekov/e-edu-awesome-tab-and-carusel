function tab_tab() {

// Scroll to Top button
    var btn = $('#button');

    $(window).scroll(function () {
        if ($(window).scrollTop() > 300) {
            btn.addClass('show');
        } else {
            btn.removeClass('show');
        }
    });

    btn.on('click', function (e) {
        e.preventDefault();
        $('html, body').animate({scrollTop: 0}, '300');
    });

// DOM is ready
    $(function () {
        // Single Page Nav
        $('#tm-nav').singlePageNav({speed: 600});

        // Smooth Scroll (https://css-tricks.com/snippets/jquery/smooth-scrolling/)
        $('a[href*="#"]')
        // Remove links that don't actually link to anything
            .not('[href="#"]')
            .not('[href="#0"]')
            .click(function (event) {
                // On-page links
                if (
                    location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '')
                    &&
                    location.hostname == this.hostname
                ) {
                    // Figure out element to scroll to
                    var target = $(this.hash);
                    target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                    // Does a scroll target exist?
                    if (target.length) {
                        // Only prevent default if animation is actually gonna happen
                        event.preventDefault();
                        $('html, body').animate({
                            scrollTop: target.offset().top
                        }, 600, function () {
                            // Callback after animation
                            // Must change focus!
                            var $target = $(target);
                            $target.focus();
                            if ($target.is(":focus")) { // Checking if the target was focused
                                return false;
                            } else {
                                $target.attr('tabindex', '-1'); // Adding tabindex for elements not focusable
                                $target.focus(); // Set focus again
                            }

                        });
                    }
                }
            });

        /* Isotope Gallery */

        // init isotope
        var $gallery = $(".tm-gallery").isotope({
            itemSelector: ".tm-gallery-item",
            layoutMode: "fitRows"
        });
        // layout Isotope after each image loads
        $gallery.imagesLoaded().progress(function () {
            $gallery.isotope("layout");
        });

        $(".filters-button-group").on("click", "a", function () {
            var filterValue = $(this).attr("data-filter");
            $gallery.isotope({filter: filterValue});
        });

        $(".tabgroup > div").hide();
        $(".tabgroup > div:first-of-type").show();
        $(".tabs a").click(function (e) {
            e.preventDefault();
            var $this = $(this),
                tabgroup = "#" + $this.parents(".tabs").data("tabgroup"),
                others = $this
                    .closest("li")
                    .siblings()
                    .children("a"),
                target = $this.attr("href");
            others.removeClass("active");
            $this.addClass("active");
        });
    });

}

tab_tab();
(function ($) {
    $('.testimonial-slider').slick({
        infinite: true,
        autoplay: false,
        // autoplay: false,
        autoplaySpeed: 2000,
        arrows: false,
        dots: false,
        slidesToShow: 1,
        slidesToScroll: 1,
        asNavFor: '.testimonial-slider-nav',
    });
    $('.testimonial-slider-nav').slick({
        arrows: false,
        dots: false,
        focusOnSelect: true,
        centerMode: true,
        centerPadding: '22px',
        slidesToShow: 5,
        asNavFor: '.testimonial-slider',
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 3,
                    // slidesToScroll: 1
                }
            }
        ]
    });
})(jQuery);